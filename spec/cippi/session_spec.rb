require 'spec_helper'

module Cippi
  RSpec.describe Session do
    describe ".from_credentials" do
      let(:client) { instance_double("Cippi::Client") }
      let(:response) { instance_double("Faraday::Response", body: body) }
      let(:body) { { "token" => "XXX" } }

      before do
        allow(Client).to receive(:new) { client }
        allow(client).to receive(:post)
          .with("login", username: "user", password: "pass") { response }
      end

      it "creates a session instance given some credentials" do
        session = Session.from_credentials("user", "pass")
        expect(session.token).to eq "XXX"
      end
    end

    context do
      subject(:session) { Session.new("XXX") }
      let(:client) { instance_double("Cippi::Client") }
      let(:response) { instance_double("Faraday::Response", body: body) }
      let(:body) { "result" }

      before do
        allow(session).to receive(:client) { client }
      end

      describe "#groups" do
        let(:body) { { "records" => [ "foo" ] } }

        before do
          allow(client).to receive(:get)
            .with("scheda-o", { foo: "bar", token: "XXX" }) { response }
        end

        it "fetches a page of results from the API" do
          expect(session.groups(foo: "bar")).to eq [ "foo" ]
        end
      end

      describe "#group" do
        before do
          allow(client).to receive(:get)
            .with("scheda-o/12", { foo: "bar", token: "XXX" }) { response }
        end

        it "fetches the result from the API" do
          expect(session.group(12, foo: "bar")).to eq "result"
        end
      end

      describe "#items" do
        let(:body) { { "records" => [ "foo" ] } }

        before do
          allow(client).to receive(:get)
            .with("scheda-oa", { foo: "bar", token: "XXX" }) { response }
        end

        it "fetches a page of results from the API" do
          expect(session.items(foo: "bar")).to eq [ "foo" ]
        end
      end

      describe "#item" do
        before do
          allow(client).to receive(:get)
            .with("scheda-oa/12", { foo: "bar", token: "XXX" }) { response }
        end

        it "fetches the result from the API" do
          expect(session.item(12, foo: "bar")).to eq "result"
        end
      end

      describe ".all_items" do
        before do
          allow(session).to receive(:items)
            .with(rows: 100, page: 1) { [ "foo" ]}
          allow(session).to receive(:items)
            .with(rows: 100, page: 2) { [ "bar" ]}
          allow(session).to receive(:items)
            .with(rows: 100, page: 3) { [] }
        end

        it "fetches multiple pages to return the entire collection" do
          expect(session.all_items).to eq [ "foo", "bar" ]
        end
      end
    end
  end
end

