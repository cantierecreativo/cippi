require 'spec_helper'

module Cippi
  RSpec.describe Client do
    subject(:client) { Client.new(host, url_prefix) }
    let(:host) { "google.com" }
    let(:url_prefix) { "/foo" }

    it "takes host and prefix" do
      expect(client.host).to eq "google.com"
      expect(client.url_prefix).to eq "/foo"
    end

    it "defaults to predefined host and prefix" do
      client = Client.new
      expect(client.host).to be_present
      expect(client.url_prefix).to be_present
    end

    context do
      before do
        stub_request(:get, "http://google.com/foo/bar?qux=1")
          .with(headers: {
            "Content-Type" => "application/json",
            "Accept"       => "application/json"
          })
          .to_return(
            status: 200,
            body: JSON.dump(response: true),
            headers: {
              "Content-Type" => "application/json"
            }
          )
      end

      describe "#get" do
        let(:request) { client.get("bar", qux: 1) }

        it "performs a GET request" do
          expect(request.body).to eq({ "response" => true })
        end
      end
    end

    context do
      before do
        stub_request(:post, "http://google.com/foo/bar")
          .with(
            headers: {
              "Content-Type" => "application/json",
              "Accept"       => "application/json"
            },
            body: JSON.dump(qux: 1)
          )
          .to_return(
            status: 200,
            body: JSON.dump(response: true),
            headers: {
              "Content-Type" => "application/json"
            }
          )
      end

      describe "#post" do
        let(:request) { client.post("bar", qux: 1) }

        it "performs a POST request" do
          expect(request.body).to eq({ "response" => true })
        end
      end
    end
  end
end

