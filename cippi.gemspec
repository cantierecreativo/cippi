# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cippi/version'

Gem::Specification.new do |spec|
  spec.name          = "cippi"
  spec.version       = Cippi::VERSION
  spec.authors       = ["Stefano Verna"]
  spec.email         = ["s.verna@cantierecreativo.net"]
  spec.summary       = %q{Api client for Museo dell'Opera, Florence}
  spec.description   = %q{Api client for Museo dell'Opera, Florence}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "webmock"


  spec.add_dependency "addressable", "~> 2.3.6"
  spec.add_dependency "faraday", "~> 0.9.0"
  spec.add_dependency "faraday_middleware", "~> 0.9.1"
  spec.add_dependency "activesupport"
  spec.add_dependency "faraday-detailed_logger"
end
