require "addressable/uri"
require "faraday"
require "faraday_middleware"
require "faraday/detailed_logger"


module Cippi
  class Client
    DEFAULT_HOST = "arnolfo.operaduomo.firenze.it"
    DEFAULT_URL_PREFIX = "/admin/rest"

    extend Forwardable
    def_delegators :connection, :get, :post

    attr_reader :host, :url_prefix

    def initialize(host = DEFAULT_HOST, url_prefix = DEFAULT_URL_PREFIX)
      @host = host
      @url_prefix = url_prefix
    end

    def connection
      options = {
        url: "https://#{host}#{url_prefix}",
        headers: {
          "Content-Type" => "application/json",
          "Accept"       => "application/json"
        }
      }

      @connection ||= Faraday.new(options) do |c|
        # c.response :detailed_logger # enable logging with this line
        c.request  :json
        c.response :json, content_type: /\bjson$/
        c.response :raise_error 
        c.adapter  :net_http
      end
    end
  end
end
