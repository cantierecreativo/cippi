require "cippi/client"

module Cippi
  class Session
    attr_reader :token, :client

    def self.from_credentials(username, password)
      request = Client.new.post(
        "login",
        username: username,
        password: password
      )

      new(request.body["token"])
    end

    def initialize(token)
      @token = token
      @client = Client.new
    end

    def all_groups(options = {})
      paginate(:groups, options)
    end

    def groups(params = {})
      get("scheda-o", params).body["records"]
    end

    def group(id, params = {})
      get("scheda-o/#{id}", params).body
    end

    def all_items(options = {})
      paginate(:items, options)
    end

    def items(params = {})
      get("scheda-oa", params).body["records"]
    end

    def item(id, params = {})
      get("scheda-oa/#{id}", params).body
    end

    private

    def get(path, params = {})
      params = params.merge(token: token)
      client.get(path, params)
    end

    def paginate(method, options)
      page = 1
      results = []
      options = { rows: 100 }.merge(options)

      loop do
        page_results = send(method, options.merge(page: page))
        break if page_results.empty?

        results += page_results
        page += 1
      end

      results
    end
  end
end

