# Cippi

Api client for Museo dell'Opera, Florence.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cippi'
```

And then execute:

```bash
  $ bundle
```

## Usage

```ruby
require "cippi"

client = Cippi::Session.from_credentials("username", "password")

client.items(rows: 20, page: 1, language: "it")
client.item(42)
client.groups(rows: 20, page: 1, language: "it")
client.group(12, language: "it")
```

To retrieve an entire collection:

```ruby
client.all_items
client.all_groups
```

## Contributing

1. Fork it ( https://github.com/[my-github-username]/cippi/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
